require_relative 'parser/segment'
require_relative 'parser/segments_collection'
require_relative 'parser/separator'
require_relative 'parser/line'

class Parser
  TAG_SEPARATOR = 'tag'.freeze
  COMMENT_SEPARATOR = 'comment'.freeze

  def initialize(markdown, options: {separator: COMMENT_SEPARATOR})
    @markdown = markdown
    @options = options
  end

  def to_json
    {
      segments: segments,
      file_path: options[:file_path]

    }
  end

  private

  attr_reader :markdown, :options

  def segments
    return [] unless markdown&.match(/\w+/)

    process_segments

    segments_collection.to_json
  end

  def process_segments
    Line.split(markdown).each do |line|
      if separator_klass.match?(line.content)
        options = separator_klass.new(line.content).extract_options

        if options
          segments_collection << Segment.new(options.merge(start_line: line.position))
        end
      end

      if segments_collection.current.nil?
        segments_collection << Segment.new(editable: true, start_line: 1)
      end

      segments_collection.current << line.content
    end
  end

  def separator_klass
    return Separator::Comment if options[:separator] == COMMENT_SEPARATOR
    return Separator::Tag if options[:separator] == TAG_SEPARATOR

    raise "Missing separator type: #{options[:separator]}"
  end

  def segments_collection
    @segments_collection ||= SegmentsCollection.new
  end
end
