class Parser
  class Segment
    def initialize(editable: true, start_line: 1)
      @editable = editable
      @start_line = start_line
      @content = []
    end

    def <<(line)
      content << line
    end

    def to_json
      {
        editable: editable,
        start_line: start_line,
        content: content.join("\n")
      }
    end

    private

    attr_reader :editable, :start_line, :content
  end
end
