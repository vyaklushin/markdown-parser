require 'json'

class Parser
  module Separator
    class Comment
      HTML_COMMENT = /(<!--.+?-->)/
      SUPPORTED_KEYS = ['editable'].freeze

      def initialize(comment)
        @comment = comment
      end

      def self.match?(string)
        string.match?(HTML_COMMENT)
      end

      def extract_options
        html_comment = comment.match(/<!--(.+?)-->/)
        return false unless html_comment

        json = JSON.parse(html_comment[1]) rescue {}

        supported_options = json.slice(*SUPPORTED_KEYS)

        return false if supported_options.empty?

        supported_options.transform_keys(&:to_sym)
      end

      private

      attr_reader :comment
    end
  end
end
