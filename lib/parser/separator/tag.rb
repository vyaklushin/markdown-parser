require 'json'

class Parser
  module Separator
    class Tag
      HEADER_TAG = /\s*\#{1,6}\s+\w+/

      def initialize(content)
        @content = content
      end

      def self.match?(string)
        string.match?(HEADER_TAG)
      end

      def extract_options
        return { editable: true } if Tag.match?(content)

        false
      end

      private

      attr_reader :content
    end
  end
end
