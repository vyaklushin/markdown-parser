class Parser
  class Line
    attr_reader :content, :position

    def initialize(content, position)
      @content = content
      @position = position
    end

    def self.split(markdown)
      markdown.split("\n").each_with_index.map do |line_content, index|
        Line.new(line_content, index + 1)
      end
    end
  end
end
