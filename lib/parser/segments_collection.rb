class Parser
  class SegmentsCollection
    def initialize
      @collection = []
    end

    def current
      collection.last
    end

    def <<(segment)
      collection << segment
    end

    def to_json
      collection.map(&:to_json)
    end

    private

    attr_reader :collection
  end
end
