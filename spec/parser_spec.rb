require 'spec_helper'
require_relative '../lib/parser'

describe Parser do
  subject(:parser) { described_class.new(markdown) }

  describe '#to_json' do
    subject { parser.to_json }

    context 'when markdown is empty' do
      let(:markdown) { nil }

      it { is_expected.to eq(file_path: nil, segments: []) }
    end

    context 'when markdown does not have html comments' do
      let(:markdown) do
        """# Header

        Content"""
      end

      it { is_expected.to eq(file_path: nil, segments: [ { content: markdown, editable: true, start_line: 1 } ]) }
    end

    context 'when markdown has html comments' do
      let(:segment_1) do
        """# Header

        Content"""
      end
      let(:segment_2) do
        """## Header 2

        More content"""
      end
      let(:comment) { "<!-- { \"editable\": false } -->" }
      let(:markdown) do
        <<~MARKDOWN
        #{segment_1}
        #{comment}
        #{segment_2}
        MARKDOWN
      end

      it 'returns two segments' do
        is_expected.to eq(
          file_path: nil,
          segments: [
            { content: segment_1, editable: true, start_line: 1 },
            { content: comment + "\n" + segment_2, editable: false, start_line: 4 },
          ]
         )
      end
    end

    context 'when markdown has html comments with wrong structure' do
      let(:segment_1) do
        """# Header

        Content"""
      end
      let(:segment_2) do
        """## Header 2

        More content"""
      end
      let(:comment) { "<!-- Just a comment -->" }
      let(:markdown) do
        <<~MARKDOWN.chomp
        #{segment_1}
        #{comment}
        #{segment_2}
        MARKDOWN
      end

      it 'returns only one segment' do
        is_expected.to eq(
          file_path: nil,
          segments: [
            { content: markdown, editable: true, start_line: 1 },
          ]
         )
      end
    end

    context 'when html comments is at the beginning' do
      let(:segment_1) do
        """# Header

        Content"""
      end
      let(:comment) { "<!-- { \"editable\": false } -->" }
      let(:markdown) do
        <<~MARKDOWN.chomp
        #{comment}
        #{segment_1}
        MARKDOWN
      end

      it 'returns only one segment' do
        is_expected.to eq(
          file_path: nil,
          segments: [
            { content: markdown, editable: false, start_line: 1 },
          ]
         )
      end
    end
  end
end
