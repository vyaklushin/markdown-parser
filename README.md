# Markdown parser 

It is a simple application that displays markdown content as a ruby hash.


```
./bin/parse -f <file_path> -s <separator>
```

```
./bin/parse -f examples/example1.md -s comment
./bin/parse -f examples/example2.md -s comment
./bin/parse -f examples/example3.md -s comment
./bin/parse -f examples/example4.md -s tag
```


```
{:segments=>
  [{:editable=>true,
    :start_line=>1,
    :content=>"# Just a simple markdown\n" + "\n" + "Does not have much"}],
 :file_path=>"examples/example1.md"}
```
