# Editable part

You can edit me

<!-- {"editable": false} -->

# Not editable part

You cannot edit me

<!-- { "editable": true } -->

Now it is editable again
